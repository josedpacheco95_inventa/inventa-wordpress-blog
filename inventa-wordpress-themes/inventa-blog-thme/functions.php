<?php
/**
 * GeneratePress child theme functions and definitions.
 *
 * Add your custom PHP in this file.
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 */

function wptheme_register_styles(){

  wp_enqueue_style('wptheme-bootstrap-icons','https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css', array(), '1.8.1','all');
  wp_enqueue_style('wptheme-bootstrap','https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', array(), '5.1.3','all');
  wp_enqueue_style('wptheme-custom',get_stylesheet_directory_uri().'/style.css', array(), '1.0','all');
}

add_action('wp_enqueue_scripts','wptheme_register_styles');

function wptheme_register_scripts(){

  wp_enqueue_script('wptheme-bootstrap','https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js',array(), '5.1.3',true);
  wp_enqueue_script('wptheme-custom',get_stylesheet_directory_uri().'/assets/js/main.js', array(), '1.0',true);


}

add_action('wp_enqueue_scripts','wptheme_register_scripts');

?>
