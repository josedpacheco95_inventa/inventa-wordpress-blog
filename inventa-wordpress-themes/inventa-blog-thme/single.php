<?php
    get_header();
?>
    <article class="content">
    <div class='py-5 inventa-single-post px-2;'>
    <?php
        if ( have_posts() ) {
            while( have_posts()) {
                the_post();
                the_content();
            }
        }
    ?>
    </div>
    <ul id='related-post-section' class='d-flex flex-column'>
        <li class='d-flex flex-row'>
            <h2 class='related-post-label'>Posts Relacionados</h2>
            <a class='text-decoration-none cursor-pointer ms-auto me-5' href='https://blog.inventashop.com.br'><span class='related-post-link'>Ver tudo</span></a>
        </li>
        <li class='row p-5'>
            <?php
                $args = array(
                    'numberposts'	=> 3,
                );
                $my_posts = get_posts( $args );
                if( ! empty( $my_posts ) ){
            
                    foreach ( $my_posts as $p ){
                        $title = $p -> post_title;
                        $url = get_the_post_thumbnail_url($p);
                        $link = get_permalink( $p );   
                        echo '<div class="col-12 col-md-6 col-lg-4 d-flex flex-column align-items-center">';
                        if ($url != '' ) {
                            echo '<a class="cursor-pointer mb-3" href="';
                            echo $link;
                            echo '" target="_blank""><div style="height:175px; width:315px; background-color: black;">';
                            echo '<img src="';
                            echo $url;
                            echo '" style="height:175px; width:315px;" /> </div> </a>';
                        } else {
                            echo '<a class="cursor-pointer mb-3" href="';
                            echo $link;
                            echo '" target="_blank">';
                            echo '<div style="height:175px; width:315px; background-color: black;"></div></a>';
                        }
                        echo '<a class="cursor-pointer text-decoration-none" href="';
                        echo $link;
                        echo '" target="_blank">';
                        echo '<span class="fw-bold mx-3 mb-3" style="color: rgb(47, 46, 46);font-size:18px;">';
                        echo $p->post_title;
                        echo '</span></a>';
                        echo '</div>';
                    }
                }
            ?>
        </li>
    </ul>
    <article>

<?php
    get_footer();
?>