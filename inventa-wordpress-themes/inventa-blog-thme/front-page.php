<?php
    get_header();
  ?>

  <div>
    <?php
        /**
            Custom Query for Sticky Posts only
            ----------------------------------
        **/
        $sticky = get_option( 'sticky_posts' );
        $args = array(
            'posts_per_page'      => 1, // Number of sticky posts to get
            'post__in'            => $sticky,
            'ignore_sticky_posts' => 1
            );

        if ( !empty($sticky) ):
            // has sticky posts
            query_posts($args);

        $stickyPosts = new WP_query();
        $stickyPosts->query($args);
            if ( $stickyPosts->have_posts() ):
    ?>
      <section 
        id="highlight-post" 
        class="container-fluid d-flex flex-column justify-content-end p-5" 
        style="<?php 
            $img_url = get_the_post_thumbnail_url(get_the_ID());
            if ($img_url != '') {
                echo 'background: url(';
                echo $img_url;
                echo ');';
                echo '-webkit-background-origin:border;';
                echo 'background-size: cover; background-repeat: no-repeat;background-position: center;';
            } else {
                echo 'background-color:darkgray';
            }
            ?>
        ">
        <?php
            while ( $stickyPosts->have_posts() ) : $stickyPosts->the_post();
        ?>
            <span class="hl-post-date"> <?php echo get_the_date( 'D M j');?> </span>
            <a class="cursor-pointer text-decoration-none" href="<?php the_permalink();?>">
                <span class="hl-post-title"><?php the_title();?></span>
            </a>    
        </section>
    <?php
            endwhile; //end loop for sticky posts
            endif; //have_posts()
            endif;
    ?>
    <div class="row p-5">
        <?php
            $args = array(
                'numberposts'	=> 20,
            );
            $my_posts = get_posts( $args );
            if( ! empty( $my_posts ) ){
        
                foreach ( $my_posts as $p ){
                    $title = $p -> post_title;
                    $url = get_the_post_thumbnail_url($p);
                    $link = get_permalink( $p );   
                    echo '<div class="col-12 col-md-6 col-lg-4 d-flex flex-column ">';
                    if ($url != '' ) {
                        echo '<a class="cursor-pointer" href="';
                        echo $link;
                        echo '" target="_blank""><div style="height:175px; width:315px; background-color: black;">';
                        echo '<img src="';
                        echo $url;
                        echo '" style="height:175px; width:315px;" /> </div> </a>';
                    } else {
                        echo '<a class="cursor-pointer" href="';
                        echo $link;
                        echo '" target="_blank">';
                        echo '<div style="height:175px; width:315px; background-color: black;"></div></a>';
                    }
                    echo '<span class="m-3" style="color: #2f2E2E; font-size: 12px;">'; 
                    echo $post_date = get_the_date( 'D M j', $p );
                    echo '</span>';
                    echo '<a class="cursor-pointer text-decoration-none" href="';
                    echo $link;
                    echo '" target="_blank">';
                    echo '<span class="fw-bold mx-3 mb-3" style="color: rgb(47, 46, 46);font-size:18px;">';
                    echo $p->post_title;
                    echo '</span></a>';
                    echo '</div>';
                }
            }
        ?>
     </div>
  </div>

  <?php
    get_footer();
  ?>